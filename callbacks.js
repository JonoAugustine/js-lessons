/*
 * This file contains examples of Callback functions.
 */

/**
 *
 * This function takes 2 number and a callback function as parameters.
 * The 2 numbers are added together, then the callback function is called.
 * The sum of the 2 numbers is passed to the callback function as a parameter.
 *
 * @param {number} a
 * @param {number} b
 * @param {function} cb - The callback which is called.
 * The callback has the sum of `a` and `b` passed as a parameter.
 *
 * @returns {*} Passes along the return of the callback.
 */
const addThenCallback = (a, b, cb) => {
  return cb(a + b);
};
