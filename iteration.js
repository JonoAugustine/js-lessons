// This file contains examples of iteration in JS

//
// For & While loops
// Classic C-family syntax...mostly

// While Loop
let check_while = true;
while (check_while) {
  console.log("While-Looping");
  check_while = false;
}

// Do-While
// Do whiles are like while loops,
// but they ensure the logic is run at least once
let check_do = true;
do {
  console.log("Do While, but only once!");
  check_do = false;
} while (check_do);

// Simple For Loop
for (let i = 0; i < 10; i++) {
  console.log("For Loop", i);
}

// For IN loop
// This loops through the KEYS of an object
// This works with arrays too (since they are objects in JS).
//    with an array, each key is an index number
const object_for = {
  name: "Jono",
  occupation: "redacted",
};
for (let key in object_for) {
  console.log(key + " : " + object_for[key]);
}

// For OF loop
// This loops through the VALUES of an iterable (arrays, strings)
const ofString = "abcd";
for (let value of ofString) {
  console.log(value);
}

// For Each Loop
// This iterator uses a callback function to execute logic
// on the values of an iterable
const forEachArray = ["My", "name", "is", "[redacted]"];
forEachArray.forEach((value, index, array) => {
  // the forEach callback can accept up to 3 parameters
  // 1st: the value of the iterator's current position
  console.log(
    "Current value: " + value,
    // 2nd: the index of the current position
    " Current Index: " + index,
    // 3rd: the array itself
    " The array: " + JSON.stringify(array)
  );
});

// Array map
// Mapping takes an iterable and uses a callback function to
// transform the values. The map then returns a new array
// of the transformed values
// Here we will map an array of strings to an array of objects
// and transform it into an array of strings
const mapLoop = [{ name: "Jono" }, { name: "Sam" }];
const mappedArray = mapLoop.map((value, index, arr) => {
  // Just like forEach, we are passed value, index, and the array
  // We return the value we want to add to the array,
  // here we want to extract the name
  return value.name;
});

// Array reduce
// reducers take an iterable and use a callback to
// reduce the data to a single value
// Here we will reduce an array of strings to a single string
const reduceArray = ["Hi", "welcome", "to", "the", "farm,", "Yeehaw"];
const reduced = reduceArray.reduce(
  (previousValue, currentValue, index, array) => {
    // The reduce function has a *lot* going on
    // It passes the index and array like forEach and map
    // The 2 important values it passes are: previousValue and currentValue
    // The previousValue can be refered to as the "sum"
    // It is the value which is being built upon each iteration
    // On the first iteration, it is the initial value
    //    or the first array value if no inital is given
    //    if no initialValue is given, the first currentvalue is the second array value

    // Return the new "sum"
    return previousValue + " " + currentValue;
  },
  // The second parameter is the initial value of "previousValue"
  ""
);

console.log(reduced);

// You could also chain maps and reducers
const initialArray = [
  { value: "My age is" },
  { value: 1 },
  { value: "[redacted]" },
];

const chained = initialArray
  .map((obj) => obj.value)
  .reduce((sum, curr) => sum + " " + curr);

console.log(chained);
